%module pymmm

%{
	#include "MMM/Model/Model.h"
	#include "MMM/Model/ModelProcessor.h"
	#include "MMM/Model/ModelProcessorWinter.h"
	#include "MMM/Model/ModelReaderXML.h"
	#include "MMM/Motion/MotionReaderXML.h"
	#include "MMM/Motion/Motion.h"
	#include "MMM/Motion/MotionFrame.h"
	#include "MMM/Motion/MotionFrameZmpNecessities.h"
	#include "MMMSimoxTools/MMMSimoxTools.h"
	#include "MMM/MathTools.h"
%}

%include "std_string.i"
%include "std_vector.i"

namespace std {
    %template(StdVectorFloat) vector<float>;
    %template(StdVectorString) vector<std::string>;
}

%include "boost_shared_ptr.i"
%shared_ptr(MMM::Motion)
%shared_ptr(MMM::MotionFrame)
%shared_ptr(MMM::MotionFrameEntry)
%shared_ptr(MMM::Model)
%shared_ptr(MMM::ModelProcessor)
%shared_ptr(MMM::MotionFrameZmpNecessities)

%template(MotionPtr) boost::shared_ptr<MMM::Motion>;
%template(MotionFramePtr) boost::shared_ptr<MMM::MotionFrame>;
%template(MotionFrameEntryPtr) boost::shared_ptr<MMM::MotionFrameEntry>;
%template(ModelPtr) boost::shared_ptr<MMM::Model>;
%template(ModelProcessorPtr) boost::shared_ptr<MMM::ModelProcessor>;
%template(MotionFrameZmpNecessitiesPtr) boost::shared_ptr<MMM::MotionFrameZmpNecessities>;

%include <eigen.i>

%eigen_typemaps(Eigen::Vector3f)
%eigen_typemaps(Eigen::Matrix4f)

// Rename all methods called "print" to "print_instance" because print is a reserved keyword in Python
%rename(print_instance) print;

namespace VirtualRobot {
	class Robot;	
	typedef boost::shared_ptr<Robot> RobotPtr;
}

namespace MMM {
	class Motion;
	typedef boost::shared_ptr<Motion> MotionPtr;

	class MotionFrame;
	typedef boost::shared_ptr<MotionFrame> MotionFramePtr;

	class MotionFrameEntry;
	typedef boost::shared_ptr<MotionFrameEntry> MotionFrameEntryPtr;

	class MotionFrameZmpNecessities;
	typedef boost::shared_ptr<MotionFrameZmpNecessities> MotionFrameZmpNecessitiesPtr;

	class Model;
	typedef boost::shared_ptr<Model> ModelPtr;

	class ModelProcessor;
	typedef boost::shared_ptr<ModelProcessor> ModelProcessorPtr;

	typedef std::vector<MotionPtr> MotionList;

	class MotionReaderXML {
	public:
		MotionReaderXML();
		MotionPtr loadMotion(const std::string &xmlFile, const std::string &motionName = std::string());
		MotionList loadAllMotions(const std::string &xmlFile);
		MotionList loadAllMotionsFromString(const std::string &xmlDataString);
		std::vector<std::string> getMotionNames(const std::string &xmlFile) const;
	};

	typedef boost::shared_ptr<MotionReaderXML> MotionReaderXMLPtr;

	class Motion {
	public:
		Motion(const std::string& name);
		Motion(const Motion& m);
		bool addMotionFrame(MotionFramePtr md);
		bool removeMotionFrame(size_t frame);
		bool setJointOrder(const std::vector<std::string> &jointNames);
		bool setSensorOrder(const std::vector<std::string> sensorNames);
		void setName(const std::string& name);
		std::string getName();
		void setComment(const std::string &comment);
		void addComment(const std::string &comment);
		std::string getComment();
		std::vector<std::string> getJointNames();
		MotionFramePtr getMotionFrame(size_t frame);
		std::vector<MotionFramePtr> getMotionFrames();
		void setMotionFrames(std::vector<MotionFramePtr> &mfs){motionFrames = mfs;}
		virtual unsigned int getNumFrames();
		void setModel(ModelPtr model);
		void setModel(ModelPtr processedModel, ModelPtr originalModel);
		ModelPtr getModel(bool processedModel = true);
		const std::string& getMotionFilePath();
		void setMotionFilePath(const std::string&filepath);
		const std::string& getMotionFileName();
		void setMotionFileName(const std::string&filename);
		void setModelProcessor(ModelProcessorPtr mp);
		ModelProcessorPtr getModelProcessor();
		virtual std::string toXML();
		void print();
		void print(const std::string &filename);
		bool hasJoint( const std::string &name );
		void calculateVelocities(int method = 0);
		void calculateAccelerations(int method = 0);
		void calculateSensorAngularAccelerations(int method = 0);
		void calculateSensorAccelerations(int method = 0);
		void calculateSensorVelocities(int method = 0);
		enum jointEnum{eValues, eVelocities, eAccelerations};
		void smoothJointValues(jointEnum type, int windowSize);
		MotionPtr getSegmentMotion(size_t frame1, size_t frame2);
		MotionPtr copy();
		MotionList getSegmentMotions(std::vector<int> segmentPoints);
		void linearMomentumDerivation(int method =0);
		void angularMomentumDerivation(int method = 0);
		void localComVelocities(int method = 0);
		void calculateZMP();
		float getValue(Eigen::VectorXf &vector, int pos);
	
	};

	class MotionFrame {
	public:
		MotionFrame(unsigned int ndof);
		MotionFrame(const MotionFrame &inpSrc);
		Eigen::Matrix4f getRootPose();
		Eigen::Vector3f getRootPos();
		Eigen::Vector3f getRootPosVel();
		Eigen::Vector3f getRootPosAcc();
		Eigen::Vector3f getRootRot();
		Eigen::Vector3f getRootRotVel();
		Eigen::Vector3f getRootRotAcc();
		bool setRootPose(const Eigen::Matrix4f &pose);
		bool setRootPos(const Eigen::Vector3f &pos);
		bool setRootPosVel(const Eigen::Vector3f &posVel);
		bool setRootPosAcc(const Eigen::Vector3f &posAcc);
		bool setRootRot(const Eigen::Vector3f &rot);
		bool setRootRotVel(const Eigen::Vector3f &rotVel);
		bool setRootRotAcc(const Eigen::Vector3f &rotAcc);
		bool setSensorPositionAndEularian(Eigen::Matrix4f sensorValues, int pos);
		float timestep;
		Eigen::VectorXf joint;
		Eigen::VectorXf joint_vel;
		Eigen::VectorXf joint_acc;
		bool addEntry(const std::string &name, MotionFrameEntryPtr entry );
		bool removeEntry(const std::string &name);
		bool hasEntry(const std::string &name) const;
		MotionFrameEntryPtr getEntry(const std::string &name);
		std::string toXML();
		unsigned int getndof();
		MotionFrameZmpNecessitiesPtr getZmpEntry();
		bool addSupportPolygon(std::vector<Eigen::Vector2f> points);
		//void setCOM(Eigen::Vector3f com);
		//Eigen::Vector3f getCOM();

	protected:
		MotionFrameZmpNecessities zmpNecessitiesEntry;
	};

	class MotionFrameZmpNecessities {
	public:
		MotionFrameZmpNecessities();
		std::string returnFoundZmpNecessities();
		int number_of_relevant_segments;
		Eigen::Vector3f com;
		Eigen::VectorXf linear_momentum;
		Eigen::VectorXf linear_mom_derivative;
		Eigen::VectorXf angular_momentum;
		Eigen::VectorXf angular_mom_derivative;
		Eigen::VectorXf segment_COM;
		Eigen::VectorXf segment_COM_derivative;
		Eigen::VectorXf angular_velocity_segments;
		std::vector<Eigen::Matrix3f> rotation_matrix_segments;
		bool setAngularVelAndRotationMatrix(std::vector<Eigen::Matrix4f> rotations);
		bool setCOM(Eigen::Vector3f com);
		bool setNumberOfRelevantSegments(int seg_number);
		bool setLinearMomentum(Eigen::VectorXf com_derivative, std::vector<float> masses);
		bool setAngularMomentum(std::vector<float> segment_masses, std::vector<Eigen::Matrix3f> inertia_tensors);
		bool setSegmentsCOM(Eigen::Vector3f com, int segment);

	};
	class Model{
	public:
		Model();
		float getMass();
	};

	class ModelReaderXML {
	public:
		ModelReaderXML();
		ModelPtr loadModel(const std::string &xmlFile, const std::string& name = std::string());
	};


	class ModelProcessorWinter {
	public:
		ModelProcessorWinter();
		virtual ModelPtr convertModel(ModelPtr input);
		virtual bool setup(float height, float mass, float handLength = -1., float handWidth = -1.);
	};
	namespace Math{
		Eigen::Matrix4f rpy2eigen4f (float r, float p, float y);
		Eigen::Matrix4f quat2eigen4f( float x, float y, float z, float w );
		Eigen::Vector3f matrix3fToEulerXZY( const Eigen::Matrix3f &m);
		Eigen::Vector3f matrix4fToEulerXZY( const Eigen::Matrix4f &m);
		Eigen::VectorXf matrix4fToPoseEulerXZY(const Eigen::Matrix4f &m);
		Eigen::VectorXf matrix4fToPoseRPY(const Eigen::Matrix4f &m);
		Eigen::Matrix3f eulerXZYToMatrix3f( const Eigen::Vector3f &eulerXZY );
		Eigen::Matrix4f poseEulerXZYToMatrix4f( const Eigen::Vector3f &pos, const Eigen::Vector3f &eulerXZY );
		Eigen::Matrix4f poseRPYToMatrix4f( const Eigen::Vector3f &pos, const Eigen::Vector3f &rpy );

		

	};
	namespace SimoxTools {
		VirtualRobot::RobotPtr buildModel(MMM::ModelPtr model, bool loadVisualizations = true);
	}
}
