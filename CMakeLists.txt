project(PySimox)

cmake_minimum_required(VERSION 2.8)

find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})

find_package(PythonLibs 2.7 REQUIRED)
include_directories(${PYTHON_INCLUDE_PATH})

find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})

find_package(Simox REQUIRED)
include_directories(${Simox_INCLUDE_DIRS})

find_package(MMMCore REQUIRED)
include_directories(${MMMCORE_INCLUDE_DIRS})

find_package(MMMTools REQUIRED)
include_directories(${MMMTOOLS_INCLUDE_DIRS})

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_SWIG_FLAGS -c++ -Wall)

set_source_files_properties(pymmm.i PROPERTIES CPLUSPLUS ON)
set_source_files_properties(pymmm.i PROPERTIES SWIG_FLAGS "-includeall")

swig_add_module(pymmm python pymmm.i)
swig_link_libraries(pymmm ${PYTHON_LIBRARIES} ${Simox_LIBRARIES} ${MMMCORE_LIBRARIES} ${MMMTOOLS_LIBRARIES})

